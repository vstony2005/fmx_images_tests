program ChangePage;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  System.StartUpCopy,
  FMX.Forms,
  fMain in 'fMain.pas' {FrmMain},
  frGame in 'frGame.pas' {FramGame: TFrame},
  frOptions in 'frOptions.pas' {FramOptions: TFrame},
  frResult in 'frResult.pas' {FramResult: TFrame},
  fMain2 in 'fMain2.pas' {FrmMain2};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFrmMain2, FrmMain2);
  Application.Run;
end.
