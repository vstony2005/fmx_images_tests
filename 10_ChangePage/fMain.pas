unit fMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, frResult, frOptions,
  frGame, FMX.Ani;

type
  TFrmMain = class(TForm)
    LytMain: TLayout;
    Label1: TLabel;
    Layout1: TLayout;
    btnGame: TButton;
    btnOptions: TButton;
    btnClose: TButton;
    LytGame: TLayout;
    LytOptions: TLayout;
    LytLoose: TLayout;
    LytWin: TLayout;
    FramGame1: TFramGame;
    FramOptions1: TFramOptions;
    FramResLoose: TFramResult;
    FramResWin: TFramResult;
    AnimCome: TFloatAnimation;
    AnimLeave: TFloatAnimation;
    procedure AnimComeFinish(Sender: TObject);
    procedure AnimLeaveFinish(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnGameClick(Sender: TObject);
    procedure btnOptionsClick(Sender: TObject);
  private
    FCurrentPage: TLayout;
    FLytLeave: TLayout;
    FLytCome: TLayout;
    procedure HideAllPages;
    procedure GoMain(Sender: TObject);
    procedure GoGame(Sender: TObject);
    procedure GoOptions(Sender: TObject);
    procedure GoLoose(Sender: TObject);
    procedure GoWin(Sender: TObject);
    procedure SetCurrentPage(const Value: TLayout);
    property CurrentPage: TLayout read FCurrentPage write SetCurrentPage;
  public
  end;

var
  FrmMain: TFrmMain;

implementation

{$R *.fmx}

procedure TFrmMain.AnimComeFinish(Sender: TObject);
begin
  FLytCome.Visible := True;
  AnimCome.Stop;
end;

procedure TFrmMain.AnimLeaveFinish(Sender: TObject);
begin
  FLytLeave.Visible := False;
  AnimLeave.Stop;
end;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  HideAllPages;
  FCurrentPage := LytMain;
  FCurrentPage.Visible := True;

  FramGame1.OnClickLoose := GoLoose;
  FramGame1.OnClickWin := GoWin;

  FramOptions1.OnClickMain := GoMain;

  FramResLoose.Text := 'Loose!';
  FramResLoose.OnClickQuit := GoMain;
  FramResLoose.OnClickReplay := GoGame;

  FramResWin.Text := 'Win!';
  FramResWin.OnClickQuit := GoMain;
  FramResWin.OnClickReplay := GoGame;

  LytMain.Tag := 1;
  LytOptions.Tag := 2;
  LytGame.Tag := 3;
  LytWin.Tag := 4;
  LytLoose.Tag := 5;

  AnimCome.PropertyName := 'Position.Y';
  AnimLeave.PropertyName := 'Position.Y';
end;

procedure TFrmMain.GoMain(Sender: TObject);
begin
  CurrentPage := LytMain;
end;

procedure TFrmMain.GoOptions(Sender: TObject);
begin
  CurrentPage := LytOptions;
end;

procedure TFrmMain.GoWin(Sender: TObject);
begin
  CurrentPage := LytWin;
end;

procedure TFrmMain.GoGame(Sender: TObject);
begin
  CurrentPage := LytGame;
end;

procedure TFrmMain.GoLoose(Sender: TObject);
begin
  CurrentPage := LytLoose;
end;

procedure TFrmMain.HideAllPages;
var
  i: Integer;
begin
  for i:=0 to ChildrenCount-1 do
  begin
    if (Children[i] is TLayout) then
      (Children[i] as TLayout).Visible := False;
  end;
end;

procedure TFrmMain.SetCurrentPage(const Value: TLayout);
begin
  {$IFDEF NO_ANIMATION}
  // change page without animation
  if Assigned(FCurrentPage) then
    FCurrentPage.Visible := False;
  FCurrentPage := Value;
  FCurrentPage.Visible := True;
  {$ELSE}
  FLytLeave := FCurrentPage;
  FCurrentPage := Value;
  FLytCome := FCurrentPage;

  AnimCome.Parent := FLytCome;
  AnimLeave.Parent := FLytLeave;

  FLytCome.Position.Y := 0;
  FLytCome.Position.Y := 0;
  AnimCome.StopValue := 0;
  AnimLeave.StartValue := 0;

  if (FLytCome.Tag > FLytLeave.Tag) then
    AnimCome.StartValue := FLytCome.Height
  else
    AnimCome.StartValue := -FLytCome.Height;

  AnimLeave.StopValue := -AnimCome.StartValue;

  FLytCome.Visible := True;

  AnimCome.Start;
  AnimLeave.Start;
  {$ENDIF NO_ANIMATION}
end;

procedure TFrmMain.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmMain.btnGameClick(Sender: TObject);
begin
  GoGame(btnGame);
end;

procedure TFrmMain.btnOptionsClick(Sender: TObject);
begin
  GoOptions(btnOptions);
end;

end.
