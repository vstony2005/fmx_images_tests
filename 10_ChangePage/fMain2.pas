unit fMain2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  frGame, frOptions, frResult, FMX.Ani, FMX.StdCtrls, FMX.Controls.Presentation;

type
  TFrameClass = class of TFrame;
  PFrame = ^TFrame;

  TFrmMain2 = class(TForm)
    LytMain: TLayout;
    AnimCome: TFloatAnimation;
    AnimLeave: TFloatAnimation;
    Layout1: TLayout;
    btnGame: TButton;
    btnOptions: TButton;
    btnClose: TButton;
    Label1: TLabel;
    procedure AnimComeFinish(Sender: TObject);
    procedure AnimLeaveFinish(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnGameClick(Sender: TObject);
    procedure btnOptionsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FramGame1: TFramGame;
    FramOptions1: TFramOptions;
    FramResLoose: TFramResult;
    FramResWin: TFramResult;

    LytGame: TLayout;
    LytOptions: TLayout;
    LytLoose: TLayout;
    LytWin: TLayout;

    /// <summary>
    ///   Page currently displayed
    /// </summary>
    FCurrentPage: TLayout;
    /// <summary>
    ///   Layout to close
    /// </summary>
    FLytLeave: TLayout;
    /// <summary>
    ///   Layout to display
    /// </summary>
    FLytCome: TLayout;

    /// <summary>
    ///   Add TFrame in a layout in this form
    /// </summary>
    function AddFrame(AFrameClass: TFrameClass; AFrame: PFrame): TLayout;
    procedure GoMain(Sender: TObject);
    procedure GoGame(Sender: TObject);
    procedure GoOptions(Sender: TObject);
    procedure GoLoose(Sender: TObject);
    procedure GoWin(Sender: TObject);
    /// <summary>
    ///   Change page with an animation
    /// </summary>
    procedure SetCurrentPage(const Value: TLayout);
    /// <summary>
    ///   Page displayed in the form
    /// </summary>
    property CurrentPage: TLayout read FCurrentPage write SetCurrentPage;
  public
  end;

var
  FrmMain2: TFrmMain2;

implementation

{$R *.fmx}

{ TFrmMain2 }

procedure TFrmMain2.FormCreate(Sender: TObject);
begin
  FCurrentPage := LytMain;

  LytOptions := AddFrame(TFramOptions, @FramOptions1);
  LytGame := AddFrame(TFramGame, @FramGame1);
  LytWin := AddFrame(TFramResult, @FramResWin);
  LytLoose := AddFrame(TFramResult, @FramResLoose);

  FramGame1.OnClickLoose := GoLoose;
  FramGame1.OnClickWin := GoWin;

  FramOptions1.OnClickMain := GoMain;

  FramResLoose.Text := 'Loose!';
  FramResLoose.OnClickQuit := GoMain;
  FramResLoose.OnClickReplay := GoGame;

  FramResWin.Text := 'Win!';
  FramResWin.OnClickQuit := GoMain;
  FramResWin.OnClickReplay := GoGame;

  AnimCome.Duration := 0.3;
  AnimLeave.Duration := 0.3;
  AnimCome.PropertyName := 'Position.Y';
  AnimLeave.PropertyName := 'Position.Y';
end;

procedure TFrmMain2.AnimComeFinish(Sender: TObject);
begin
  FLytCome.Visible := True;
  AnimCome.Stop;
end;

procedure TFrmMain2.AnimLeaveFinish(Sender: TObject);
begin
  FLytLeave.Visible := False;
  AnimLeave.Stop;
end;

procedure TFrmMain2.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmMain2.btnGameClick(Sender: TObject);
begin
  GoGame(btnGame);
end;

procedure TFrmMain2.btnOptionsClick(Sender: TObject);
begin
  GoOptions(btnOptions);
end;

procedure TFrmMain2.SetCurrentPage(const Value: TLayout);
begin
  FLytLeave := FCurrentPage;
  FCurrentPage := Value;
  FLytCome := FCurrentPage;

  AnimCome.Parent := FLytCome;
  AnimLeave.Parent := FLytLeave;

  FLytCome.Position.Y := 0;
  FLytCome.Position.Y := 0;
  AnimCome.StopValue := 0;
  AnimLeave.StartValue := 0;

  if (FLytCome.Tag > FLytLeave.Tag) then
    AnimCome.StartValue := FLytCome.Height
  else
    AnimCome.StartValue := -FLytCome.Height;

  AnimLeave.StopValue := -AnimCome.StartValue;

  FLytCome.Visible := True;

  AnimCome.Start;
  AnimLeave.Start;
end;

function TFrmMain2.AddFrame(AFrameClass: TFrameClass; AFrame: PFrame): TLayout;
begin
  Result := TLayout.Create(Self);
  Result.Parent := Self;
  Result.Align := TAlignLayout.Client;
  Result.Visible := False;
  Result.Tag := Self.ChildrenCount;
  Result.Name := Format('MyLyt_%d', [Result.Tag]);

  AFrame^ := AFrameClass.Create(Result);
  AFrame.Parent := Result;
  AFrame.Name := Format('MyFrame_%d', [Result.Tag]);
  AFrame.Align := TAlignLayout.Client;
end;

procedure TFrmMain2.GoMain(Sender: TObject);
begin
  CurrentPage := LytMain;
end;

procedure TFrmMain2.GoOptions(Sender: TObject);
begin
  CurrentPage := LytOptions;
end;

procedure TFrmMain2.GoGame(Sender: TObject);
begin
  CurrentPage := LytGame;
end;

procedure TFrmMain2.GoWin(Sender: TObject);
begin
  CurrentPage := LytWin;
end;

procedure TFrmMain2.GoLoose(Sender: TObject);
begin
  CurrentPage := LytLoose;
end;

end.
