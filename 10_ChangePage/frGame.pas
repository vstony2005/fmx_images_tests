unit frGame;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, FMX.Controls.Presentation;

type
  TFramGame = class(TFrame)
    Label1: TLabel;
    btnLoose: TButton;
    btnWin: TButton;
    LytFooter: TLayout;
    procedure btnLooseClick(Sender: TObject);
    procedure btnWinClick(Sender: TObject);
  private
    FOnClickLoose: TNotifyEvent;
    FOnClickWin: TNotifyEvent;
  public
  published
    property OnClickLoose: TNotifyEvent read FOnClickLoose write FOnClickLoose;
    property OnClickWin: TNotifyEvent read FOnClickWin write FOnClickWin;
  end;

implementation

{$R *.fmx}

procedure TFramGame.btnLooseClick(Sender: TObject);
begin
  if Assigned(FOnClickLoose) then
    FOnClickLoose(Self);
end;

procedure TFramGame.btnWinClick(Sender: TObject);
begin
  if Assigned(FOnClickWin) then
    FOnClickWin(Self);
end;

end.
