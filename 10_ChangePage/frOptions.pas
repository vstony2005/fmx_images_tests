unit frOptions;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Layouts;

type
  TFramOptions = class(TFrame)
    Label1: TLabel;
    LytFooter: TLayout;
    btnMain: TButton;
    procedure btnMainClick(Sender: TObject);
  private
    FOnClickMain: TNotifyEvent;
  public
  published
    property OnClickMain: TNotifyEvent read FOnClickMain write FOnClickMain;
  end;

implementation

{$R *.fmx}

procedure TFramOptions.btnMainClick(Sender: TObject);
begin
  if Assigned(OnClickMain) then
    OnClickMain(Self);
end;

end.
