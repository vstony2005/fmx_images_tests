unit frResult;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Layouts;

type
  TFramResult = class(TFrame)
    Label1: TLabel;
    Label2: TLabel;
    LytFooter: TLayout;
    btnReplay: TButton;
    btnQuit: TButton;
    procedure btnQuitClick(Sender: TObject);
    procedure btnReplayClick(Sender: TObject);
  private
    FOnClickQuit: TNotifyEvent;
    FOnClickReplay: TNotifyEvent;
    FText: string;
    procedure SetText(const Value: string);
  public
    property Text: string read FText write SetText;
  published
    property OnClickQuit: TNotifyEvent read FOnClickQuit write FOnClickQuit;
    property OnClickReplay: TNotifyEvent read FOnClickReplay write FOnClickReplay;
  end;

implementation

{$R *.fmx}

{ TFramResult }

procedure TFramResult.SetText(const Value: string);
begin
  FText := Value;
  Label1.Text := Value;
end;

procedure TFramResult.btnQuitClick(Sender: TObject);
begin
  if Assigned(FOnClickQuit) then
    OnClickQuit(Self);
end;

procedure TFramResult.btnReplayClick(Sender: TObject);
begin
  if Assigned(FOnClickReplay) then
    OnClickReplay(Self);
end;

end.
