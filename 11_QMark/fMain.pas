unit fMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects;

type
  TForm1 = class(TForm)
    Path1: TPath;
    Path2: TPath;
    RectQuestionMark: TRectangle;
    RectForClick: TRectangle;
    procedure FormCreate(Sender: TObject);
    procedure RectForClickMouseEnter(Sender: TObject);
    procedure RectForClickMouseLeave(Sender: TObject);
  private
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.FormCreate(Sender: TObject);
begin
  Path1.Fill.Color := TAlphaColorRec.Black;
  Path2.Fill.Color := TAlphaColorRec.Black;

  Path1.Fill.Kind := TBrushKind.None;
  Path2.Fill.Kind := TBrushKind.None;
end;

procedure TForm1.RectForClickMouseEnter(Sender: TObject);
begin
  Path1.Fill.Kind := TBrushKind.Solid;
  Path2.Fill.Kind := TBrushKind.Solid;
end;

procedure TForm1.RectForClickMouseLeave(Sender: TObject);
begin
  Path1.Fill.Kind := TBrushKind.None;
  Path2.Fill.Kind := TBrushKind.None;
end;

end.
