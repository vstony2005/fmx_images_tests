unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Controls.Presentation, FMX.StdCtrls;

type
  TForm1 = class(TForm)
    rec1: TRectangle;
    rec2: TRectangle;
    btn1: TButton;
    procedure FormKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure rec2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Déclarations privées }
    FVal: Integer;
    FPress: Boolean;
  public
    { Déclarations publiques }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.btn1Click(Sender: TObject);
(*
  création manuelle de composants
*)
const
  NB_LIN: Integer = 4;
  NB_COL: Integer = 8;
var
  i, j: Integer;
  c: TCircle;
  l: TLabel;
begin
  for i := 0 to NB_COL - 1 do
    for j := 0 to NB_LIN - 1 do
    begin
      c := TCircle.Create(Self);
      c.Parent := Self;
      c.Position.X := 5 * (1 + i) + c.Size.Width * i;
      c.Position.Y := 5 * (1 + j) + c.Size.Height * j;

      (*
        c.Margins.Bottom := 5;
        c.Margins.Left := 5;
        c.Margins.Right := 5;
        c.Margins.Top := 5;
      *)

      l := TLabel.Create(c);
      l.Parent := c;
      l.Align := TAlignLayout.Client;
      l.Text := IntToStr((i + 1) + (j) * NB_COL);
      l.TextSettings.HorzAlign := TTextAlign.Center;

      l.StyledSettings := l.StyledSettings - [TStyledSetting.Style,
        TStyledSetting.Size];

      l.TextSettings.Font.Style := [TFontStyle.fsBold];
      l.TextSettings.Font.Size := 20;
      l.Font.Size := 20;
    end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  FVal := 0;
  FPress := False;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char;
  Shift: TShiftState);
(*
  prendre en compte un seul appuie sur une touche
*)
begin
  if (FPress) then
    Exit;

  FPress := not FPress;
  FVal := FVal + 1;
  Self.Caption := FVal.ToString;
end;

procedure TForm1.FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
  Shift: TShiftState);
begin
  FPress := not FPress;
end;

procedure TForm1.rec2Click(Sender: TObject);
(*
  afficher la partie d'une image
*)
var
  img1, img2: TBitmap;
  arec: TRect;
begin
  try
    img1 := TBitmap.CreateFromFile('..\..\..\imgs\grille.png');
    img2 := TBitmap.Create(50, 50);

    try
      arec := Rect(340, 390, 340 + 50, 390 + 50);

      img2.CopyFromBitmap(img1, arec, 0, 0);

      rec2.Fill.Bitmap.Bitmap.Assign(img2);
      rec2.Fill.Kind := TBrushKind.Bitmap;
    finally
      img1.Free;
      img2.Free;
    end;
  except
    on e: Exception do
  end;
end;

end.
