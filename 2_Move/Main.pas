unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Ani;

type
  TForm1 = class(TForm)
    rectPerso: TRectangle;
    FloatAnimation1: TFloatAnimation;
    rectFond: TRectangle;
    procedure rectPersoClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Déclarations privées }
    FMove: Boolean;
  public
    { Déclarations publiques }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.FormCreate(Sender: TObject);
begin
  FMove := False;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char;
  Shift: TShiftState);
begin
  if (FMove) then
    Exit;

  FMove := True;

  case Key of
    vkUp:
      if (rectPerso.Position.Y > rectPerso.Size.Height) then
        rectPerso.Position.Y := rectPerso.Position.Y - rectPerso.Size.Height;
    vkRight:
      if (rectPerso.Position.X + rectPerso.Size.Width * 2 < rectFond.Width) then
        rectPerso.Position.X := rectPerso.Position.X + rectPerso.Size.Width;
    vkDown:
      if (rectPerso.Position.Y + rectPerso.Size.Height * 2 < rectFond.Height) then
        rectPerso.Position.Y := rectPerso.Position.Y + rectPerso.Size.Height;
    vkLeft:
      if (rectPerso.Position.X > rectPerso.Size.Width) then
        rectPerso.Position.X := rectPerso.Position.X - rectPerso.Size.Width;
  else
  end;
end;

procedure TForm1.FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
  Shift: TShiftState);
begin
  FMove := False;
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  if (rectPerso.Position.X + rectPerso.Size.Width > rectFond.Size.Width) then
    rectPerso.Position.X := rectFond.Size.Width - rectPerso.Size.Width;
  if (rectPerso.Position.Y + rectPerso.Size.Height > rectFond.Size.Height) then
    rectPerso.Position.Y := rectFond.Size.Height - rectPerso.Size.Height;

end;

procedure TForm1.rectPersoClick(Sender: TObject);
begin
  if (FloatAnimation1.Running) then
    FloatAnimation1.Stop
  else
    FloatAnimation1.Start;
end;

end.
