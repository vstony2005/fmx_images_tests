unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  System.ImageList, FMX.ImgList, FMX.Controls.Presentation, FMX.StdCtrls;

type
  TForm2 = class(TForm)
    Rectangle1: TRectangle;
    ImageList1: TImageList;
    btnMultiRes: TButton;
    btnBVyName: TButton;
    btnFusion: TButton;
    procedure btnMultiResClick(Sender: TObject);
    procedure btnBVyNameClick(Sender: TObject);
    procedure btnFusionClick(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

uses
  FMX.MultiResBitmap;

procedure TForm2.btnMultiResClick(Sender: TObject);
const
  SCALE = 1;
var
  img: TBitmap;

  srcImg: TCustomSourceItem;
  bmpItm: TCustomBitmapItem;

  bmpTmp: TBitmap;

  Size: TSize;
  src, trg: TRectF;

  rec: TRectangle;
begin
  // select image by item index
  srcImg := ImageList1.Source.Items[0];
  bmpItm := srcImg.MultiResBitmap.ItemByScale(SCALE, True, True);

  bmpTmp := bmpItm.Bitmap;
  src := TRectF.Create(0, 0, bmpTmp.Width, bmpTmp.Height);
  trg := Rect(0, 0, 50, 50);

  img := TBitmap.Create(50, 50);
  img.Canvas.BeginScene;
  img.Canvas.DrawBitmap(bmpTmp, src, trg, 1);
  img.Canvas.EndScene;

  rec := TRectangle.Create(Self);
  rec.Parent := Self;
  rec.Position.X := 150;
  rec.Position.Y := 150;
  rec.Fill.Kind := TBrushKind.Bitmap;
  rec.Fill.Bitmap.WrapMode := TWrapMode.TileStretch;
  rec.Fill.Bitmap.Bitmap.Assign(img);
  img.Free;

  rec.Stroke.Kind := TBrushKind.None;
end;

procedure TForm2.btnBVyNameClick(Sender: TObject);
var
  img: TBitmap;

  srcImg: TCustomSourceItem;
  bmpItm: TCustomBitmapItem;

  bmpTmp: TBitmap;

  Size: TSize;
  src, trg: TRectF;

  rec: TRectangle;
begin
  // select image by name
  ImageList1.BitmapItemByName('panda', bmpItm, Size);

  bmpTmp := bmpItm.Bitmap;
  src := TRectF.Create(0, 0, bmpTmp.Width, bmpTmp.Height);
  trg := Rect(0, 0, 50, 50);

  img := TBitmap.Create(50, 50);
  img.Canvas.BeginScene;
  img.Canvas.DrawBitmap(bmpTmp, src, trg, 1);
  img.Canvas.EndScene;

  rec := TRectangle.Create(Self);
  rec.Parent := Self;
  rec.Position.X := 200;
  rec.Position.Y := 150;
  rec.Fill.Kind := TBrushKind.Bitmap;
  rec.Fill.Bitmap.WrapMode := TWrapMode.TileStretch;
  rec.Stroke.Kind := TBrushKind.None;

  rec.Fill.Bitmap.Bitmap.Assign(img);
  img.Free;
end;

procedure TForm2.btnFusionClick(Sender: TObject);
var
  img: TBitmap;

  srcImg: TCustomSourceItem;
  bmpItm: TCustomBitmapItem;

  bmpTmp: TBitmap;

  Size: TSize;
  src, trg: TRectF;

  rec: TRectangle;
  imgName: string;
  i, posX, posY: Integer;
begin
  bmpItm := nil;

  // image creation
  img := TBitmap.Create(100, 100);

  for i := 0 to 3 do
  begin
    posX := i mod 2 * 50;
    posY := Trunc(i / 2) * 50;

    case i of
      1:
        imgName := 'panda';
      2:
        imgName := 'penguin';
      3:
        imgName := 'hippo';
    else
      imgName := 'snake';
    end;

    ImageList1.BitmapItemByName(imgName, bmpItm, Size);

    bmpTmp := bmpItm.Bitmap;
    src := TRectF.Create(0, 0, bmpTmp.Width, bmpTmp.Height);
    trg := Rect(posX, posY, posX + 50, posY + 50);
    img.Canvas.BeginScene;
    img.Canvas.DrawBitmap(bmpTmp, src, trg, 1);
    img.Canvas.EndScene;
  end;

  // superposition
  posX := 25;
  posY := 25;

  ImageList1.BitmapItemByName('snake', bmpItm, Size);

  bmpTmp := bmpItm.Bitmap;
  src := TRectF.Create(0, 0, bmpTmp.Width, bmpTmp.Height);
  trg := Rect(posX, posY, posX + 50, posY + 50);
  img.Canvas.BeginScene;
  img.Canvas.DrawBitmap(bmpTmp, src, trg, 1);
  img.Canvas.EndScene;

  // add in rectange
  rec := TRectangle.Create(Self);
  rec.Parent := Self;
  rec.Size.Height := img.Height;
  rec.Size.Width := img.Width;
  rec.Position.X := 200;
  rec.Position.Y := 250;
  rec.Fill.Kind := TBrushKind.Bitmap;
  rec.Fill.Bitmap.WrapMode := TWrapMode.TileStretch;
  rec.Stroke.Kind := TBrushKind.None;

  rec.Fill.Bitmap.Bitmap.Assign(img);
  img.Free;
end;

end.
