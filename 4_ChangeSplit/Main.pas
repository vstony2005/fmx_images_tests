unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.ImageList, FMX.ImgList, FMX.Controls.Presentation, FMX.StdCtrls,
  FMX.Objects;

type
  TForm3 = class(TForm)
    ImageList1: TImageList;
    btnLoad: TButton;
    btnShow: TButton;
    btnLoad2: TButton;
    procedure btnLoadClick(Sender: TObject);
    procedure btnShowClick(Sender: TObject);
    procedure btnLoad2Click(Sender: TObject);
  private
    { Déclarations privées }
    function AddImage(aBitmap: TBitmap; const name: string): Integer;
    procedure LoadAPack(const namePack: string);
  public
    { Déclarations publiques }
  end;

var
  Form3: TForm3;

implementation

{$R *.fmx}

uses
  FMX.MultiResBitmap;

{ TForm3 }

function TForm3.AddImage(aBitmap: TBitmap; const name: string): Integer;
const
  SCALE = 1;
var
  srcItm: TCustomSourceItem;
  bmpItm: TCustomBitmapItem;
  dest: TCustomDestinationItem;
  layer: TLayer;
begin
  Result := -1;
  if (aBitmap.Width = 0) or (aBitmap.Height = 0) then
    Exit;

  // add source bitmap
  srcItm := ImageList1.Source.Add;
  srcItm.MultiResBitmap.TransparentColor := TColorRec.Fuchsia;
  srcItm.MultiResBitmap.SizeKind := TSizeKind.Source;
  srcItm.MultiResBitmap.Width := Round(aBitmap.Width / SCALE);
  srcItm.MultiResBitmap.Height := Round(aBitmap.Height / SCALE);
  srcItm.Name := name;
  bmpItm := srcItm.MultiResBitmap.ItemByScale(SCALE, True, True);

  if not Assigned(bmpItm) then
  begin
    bmpItm := srcItm.MultiResBitmap.Add;
    bmpItm.SCALE := SCALE;
  end;

  bmpItm.Bitmap.Assign(aBitmap);

  dest := ImageList1.Destination.Add;
  layer := dest.Layers.Add;
  layer.SourceRect.Rect := TRectF.Create(TPoint.Zero,
    srcItm.MultiResBitmap.Width, srcItm.MultiResBitmap.Height);
   layer.Name := srcItm.Name;
//  ShowMessage(layer.Name);

  Result := dest.Index;
end;

procedure TForm3.btnLoad2Click(Sender: TObject);
begin
  LoadAPack('..\..\..\imgs\square.png');
end;

procedure TForm3.btnLoadClick(Sender: TObject);
begin
  LoadAPack('..\..\..\imgs\round.png');
end;

procedure TForm3.btnShowClick(Sender: TObject);
const
  SCALE = 1;
var
  img: TBitmap;

  bmpItm: TCustomBitmapItem;

  bmpTmp: TBitmap;

  Size: TSize;
  src, trg: TRectF;

  rec: TRectangle;
begin
  if (ImageList1.Count = 0) then
    Exit;

   ImageList1.BitmapItemByName('hippo', bmpItm, Size);

  bmpTmp := bmpItm.Bitmap;
  src := TRectF.Create(0, 0, bmpTmp.Width, bmpTmp.Height);
  trg := Rect(0, 0, 50, 50);

  img := TBitmap.Create(50, 50);
  img.Canvas.BeginScene;
  img.Canvas.DrawBitmap(bmpTmp, src, trg, 1);
  img.Canvas.EndScene;

  rec := TRectangle.Create(Self);
  rec.Parent := Self;
  rec.Position.X := 110;
  rec.Position.Y := 250;
  rec.Fill.Kind := TBrushKind.Bitmap;
  rec.Fill.Bitmap.WrapMode := TWrapMode.TileStretch;
  rec.Stroke.Kind := TBrushKind.None;

  rec.Fill.Bitmap.Bitmap.Assign(img);
  img.Free;
end;

procedure TForm3.LoadAPack(const namePack: string);
var
  i: Integer;
  posX, posY: Integer;
  img1, img2: TBitmap;
  arec: TRect;
  nameBmp: string;
begin
  if (ImageList1.Destination.Count > 0) then
  begin
    ImageList1.Destination.Clear;
    ImageList1.Source.Clear;
  end;

  img1 := TBitmap.CreateFromFile(namePack);

  for i := 0 to 7 do
  begin
    posY := Trunc(i / 4) * 60;
    posX := (i mod 4) * 60;
    arec := Rect(posX, posY, posX + 60, posY + 60);

    img2 := TBitmap.Create(60, 60);
    img2.CopyFromBitmap(img1, arec, 0, 0);

    case i of
      0: nameBmp := 'snake';
      1: nameBmp := 'pig';
      2: nameBmp := 'penguin';
      3: nameBmp := 'parrot';
      4: nameBmp := 'panda';
      5: nameBmp := 'monkey';
      6: nameBmp := 'rabbit';
    else
      nameBmp := 'hippo';
    end;

    AddImage(img2,nameBmp);

    with TRectangle.Create(Self) do
    begin
      Parent := Self;

      Fill.Kind := TBrushKind.Bitmap;
      Fill.Bitmap.Bitmap.Assign(img2);
      Fill.Bitmap.WrapMode := TWrapMode.TileStretch;

      Stroke.Kind := TBrushKind.None;

      Position.X := posX + 50;
      Position.Y := posY + 100;
    end;

    img2.Free;
  end;

  img1.Free;
end;

end.
