unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Ani,
  FMX.Objects, FMX.Controls.Presentation, FMX.StdCtrls;

type
  TfrmMain = class(TForm)
    Rectangle1: TRectangle;
    Label1: TLabel;
    Rectangle2: TRectangle;
    Button1: TButton;
    BitmapListAnimation1: TBitmapListAnimation;
    procedure Rectangle1Click(Sender: TObject);
    procedure BitmapListAnimation1Finish(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.fmx}

uses
  MyAssist;

procedure TfrmMain.BitmapListAnimation1Finish(Sender: TObject);
begin
  BitmapListAnimation1.Enabled := False;
end;

procedure TfrmMain.Button1Click(Sender: TObject);
var
  rec: TRectangle;
  anim: TBitmapListAnimation;
  event: procedure of object;
begin
  rec := TRectangle.Create(Self);
  rec.Parent := Self;
  rec.Align := TAlignLayout.Client;
  rec.Fill.Kind := TBrushKind.Bitmap;
  rec.Stroke.Kind := TBrushKind.None;
  rec.Fill.Bitmap.WrapMode := TWrapMode.TileStretch;

  anim := TBitmapListAnimation.Create(rec);
  anim.Parent := rec;
  anim.Enabled := False;
  anim.AnimationCount := 36;
  anim.AnimationRowCount := 6;
  anim.AnimationBitmap.Assign(BitmapListAnimation1.AnimationBitmap);
  anim.PropertyName := 'Fill.Bitmap.Bitmap';
  anim.Duration := 1.5;
  anim.OnFinish := TBitmapListAnimationAssistant.EndAnim;
  anim.Enabled := True;
end;

procedure TfrmMain.Rectangle1Click(Sender: TObject);
begin
  BitmapListAnimation1.Enabled := True;
end;

end.
