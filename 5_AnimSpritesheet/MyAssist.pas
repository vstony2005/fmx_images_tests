unit MyAssist;

interface

uses
  Fmx.Ani, System.Classes;

type
  TBitmapListAnimationAssistant = class(TBitmapListAnimation)
  public
    class procedure EndAnim(Sender: TObject);
  end;

implementation

{ TBitmapListAnimationAssistant }

class procedure TBitmapListAnimationAssistant.EndAnim(Sender: TObject);
begin
  TThread.ForceQueue(nil,
    procedure
    begin
      TBitmapListAnimation(Sender).Parent.Free;
    end);
end;

end.
