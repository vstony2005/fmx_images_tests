unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Ani,
  FMX.Objects;

type
{$SCOPEDENUMS ON}
  TDirection = (None, Down, Left, Up, Right);

  TForm1 = class(TForm)
    recFond: TRectangle;
    recPerso: TRectangle;
    idleD: TBitmapListAnimation;
    walkL: TBitmapListAnimation;
    walkR: TBitmapListAnimation;
    walkU: TBitmapListAnimation;
    walkD: TBitmapListAnimation;
    idleU: TBitmapListAnimation;
    idleR: TBitmapListAnimation;
    idleL: TBitmapListAnimation;
    ColorKeyAnimation1: TColorKeyAnimation;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure Timer1Timer(Sender: TObject);
  private
    { Déclarations privées }
    FDirection: TDirection;
    FAnim: TBitmapListAnimation;
    FIsWalk: Boolean;
    procedure StopWalk;
    procedure StartWalk;
    procedure Walk(b: Boolean);
  public
    { Déclarations publiques }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.FormCreate(Sender: TObject);
begin
  FDirection := TDirection.None;
  FAnim := idleD;
  FAnim.Enabled := True;
  FIsWalk := False;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char;
  Shift: TShiftState);
begin
  case Key of
    vkUP:
      begin
        if (FDirection = TDirection.Up) then
          Exit;
        FDirection := TDirection.Up;
      end;
    vkLeft:
      begin
        if (FDirection = TDirection.Left) then
          Exit;
        FDirection := TDirection.Left;
      end;
    vkRight:
      begin
        if (FDirection = TDirection.Right) then
          Exit;
        FDirection := TDirection.Right;
      end;
    vkDown:
      begin
        if (FDirection = TDirection.Down) then
          Exit;
        FDirection := TDirection.Down;
      end;
  else
    Exit;
  end;

  FIsWalk := True;
  StartWalk;
end;

procedure TForm1.FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
  Shift: TShiftState);
begin
  FIsWalk := False;
  StopWalk;
end;

procedure TForm1.StartWalk;
begin
  Walk(True);
end;

procedure TForm1.StopWalk;
begin
  Walk(False);
  FDirection := TDirection.None;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  case FDirection of
    TDirection.Down:
      if (recPerso.Position.Y + recPerso.Size.Height + 3 < recFond.Size.Height)
      then
        recPerso.Position.Y := recPerso.Position.Y + 3;
    TDirection.Left:
      if (recPerso.Position.X - 3 > 0) then
        recPerso.Position.X := recPerso.Position.X - 3;
    TDirection.Up:
      if (recPerso.Position.Y - 3 >= 0) then
        recPerso.Position.Y := recPerso.Position.Y - 3;
    TDirection.Right:
      if (recPerso.Position.X + recPerso.Size.Width + 3 < recFond.Size.Width)
      then
        recPerso.Position.X := recPerso.Position.X + 3;
  end;
end;

procedure TForm1.Walk(b: Boolean);
begin
  FAnim.Enabled := False;

  case FDirection of
    TDirection.Left:
      begin
        if (b) then
          FAnim := walkL
        else
          FAnim := idleL;
      end;
    TDirection.Up:
      begin
        if (b) then
          FAnim := walkU
        else
          FAnim := idleU;
      end;
    TDirection.Right:
      begin
        if (b) then
          FAnim := walkR
        else
          FAnim := idleR;
      end;
  else
    if (b) then
      FAnim := walkD
    else
      FAnim := idleD;
  end;

  FAnim.Enabled := True;
end;

end.
