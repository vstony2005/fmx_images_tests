unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.Objects, FMX.IconFontImage, System.ImageList,
  FMX.ImgList, FMX.IconFontsImageList, FMX.StdCtrls, FMX.SVGIconImageList;

type
  TForm4 = class(TForm)
    IconFontsImageList1: TIconFontsImageList;
    SpeedButton1: TSpeedButton;
    Button1: TButton;
    Glyph1: TGlyph;
    SVGIconImageList1: TSVGIconImageList;
    Glyph2: TGlyph;
    Rectangle1: TRectangle;
    procedure Rectangle1MouseEnter(Sender: TObject);
    procedure Rectangle1MouseLeave(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  Form4: TForm4;

implementation

{$R *.fmx}

procedure TForm4.Rectangle1MouseEnter(Sender: TObject);
begin
  Glyph2.ImageIndex := 1;
end;

procedure TForm4.Rectangle1MouseLeave(Sender: TObject);
begin
  Glyph2.ImageIndex := 0;
end;

procedure TForm4.SpeedButton1Click(Sender: TObject);
begin
  SpeedButton1.ImageIndex := (SpeedButton1.ImageIndex + 1) mod 4;
end;

end.
