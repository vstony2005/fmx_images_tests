program CasesImages;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  System.StartUpCopy,
  FMX.Forms,
  Main in 'Main.pas' {Form5},
  FmCase in 'FmCase.pas' {FrmCase: TFrame},
  FData in 'FData.pas' {MyDatas: TDataModule},
  FmCases in 'FmCases.pas' {FrmCases: TFrame};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm5, Form5);
  Application.CreateForm(TMyDatas, MyDatas);
  Application.Run;
end.
