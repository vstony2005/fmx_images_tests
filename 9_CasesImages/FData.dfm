object MyDatas: TMyDatas
  OldCreateOrder = False
  Height = 150
  Width = 215
  object SVGIconImageList1: TSVGIconImageList
    Source = <
      item
        MultiResBitmap = <
          item
          end>
        IconName = '02'
        SVGText = 
          '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'#10'<!-- Gene' +
          'rator: Adobe Illustrator 18.1.1, SVG Export Plug-In . SVG Versio' +
          'n: 6.00 Build 0)  -->'#10#10'<svg'#10'   version="1.1"'#10'   id="Capa_1"'#10'   x' +
          '="0px"'#10'   y="0px"'#10'   viewBox="0 0 281.232 281.232"'#10'   style="ena' +
          'ble-background:new 0 0 281.232 281.232;"'#10'   xml:space="preserve"' +
          #10'   sodipodi:docname="02.svg"'#10'   inkscape:version="1.1 (c68e22c3' +
          '87, 2021-05-23)"'#10'   xmlns:inkscape="http://www.inkscape.org/name' +
          'spaces/inkscape"'#10'   xmlns:sodipodi="http://sodipodi.sourceforge.' +
          'net/DTD/sodipodi-0.dtd"'#10'   xmlns="http://www.w3.org/2000/svg"'#10'  ' +
          ' xmlns:svg="http://www.w3.org/2000/svg"><defs'#10'   id="defs41">'#10#9#10 +
          #9#10'</defs><sodipodi:namedview'#10'   id="namedview39"'#10'   pagecolor="#' +
          'ffffff"'#10'   bordercolor="#666666"'#10'   borderopacity="1.0"'#10'   inksc' +
          'ape:pageshadow="2"'#10'   inkscape:pageopacity="0.0"'#10'   inkscape:pag' +
          'echeckerboard="0"'#10'   showgrid="false"'#10'   inkscape:zoom="1.479205' +
          '8"'#10'   inkscape:cx="98.701613"'#10'   inkscape:cy="187.60067"'#10'   inks' +
          'cape:window-width="1920"'#10'   inkscape:window-height="1017"'#10'   ink' +
          'scape:window-x="-8"'#10'   inkscape:window-y="32"'#10'   inkscape:window' +
          '-maximized="1"'#10'   inkscape:current-layer="Capa_1" />'#10'<g'#10'   id="g' +
          '1058"'#10'   inkscape:export-filename="D:\download\01.png"'#10'   inksca' +
          'pe:export-xdpi="90"'#10'   inkscape:export-ydpi="90"'#10'   style="opaci' +
          'ty:1"'#10'   transform="translate(275.82369,5.4083075)"><path'#10'     s' +
          'tyle="fill:#010002"'#10'     d="M 231.634,79.976 V 79.225 C 231.634,' +
          '30.181 192.772,0 137.32,0 105.333,0 79.905,9.018 59.536,22.98 47' +
          '.695,31.095 46.629,48.886 55.304,60.335 l 6.326,8.349 c 8.675,11' +
          '.444 24.209,12.532 36.784,5.586 11.46,-6.331 23.083,-9.758 34,-9' +
          '.758 18.107,0 28.294,7.919 28.294,20.75 v 0.375 c 0,16.225 -15.4' +
          '69,39.411 -59.231,43.181 l -1.507,1.697 c -0.832,0.936 0.218,13.' +
          '212 2.339,27.413 l 1.741,11.58 c 2.121,14.201 14.065,25.71 26.66' +
          '8,25.71 12.603,0 23.839,-5.406 25.08,-12.069 1.256,-6.668 2.268,' +
          '-12.075 2.268,-12.075 41.869,-10.192 73.568,-43.561 73.568,-91.0' +
          '98 z"'#10'     id="path2" /><path'#10'     style="fill:#010002"'#10'     d="' +
          'm 118.42,217.095 c -14.359,0 -25.993,11.64 -25.993,25.999 v 12.1' +
          '4 c 0,14.359 11.64,25.999 25.993,25.999 h 22.322 c 14.359,0 25.9' +
          '99,-11.64 25.999,-25.999 v -12.14 c 0,-14.359 -11.645,-25.999 -2' +
          '5.999,-25.999 z"'#10'     id="path4" /></g><g'#10'   id="g1062"'#10'   inksc' +
          'ape:export-filename="D:\download\02.png"'#10'   inkscape:export-xdpi' +
          '="90"'#10'   inkscape:export-ydpi="90"'#10'   transform="matrix(0.920308' +
          '07,0,0,0.95006878,310.30165,5.0947828)"><path'#10'     style="fill:n' +
          'one;fill-opacity:1;stroke:#000000;stroke-width:15;stroke-miterli' +
          'mit:4;stroke-dasharray:none;stroke-opacity:1"'#10'     d="m -93.5404' +
          '78,82.003612 v -0.751 c 0,-49.044 -38.861992,-79.2249996 -94.313' +
          '992,-79.2249996 -31.987,0 -57.415,9.0179996 -77.784,22.9799996 -' +
          '11.841,8.115 -12.907,25.906 -4.232,37.355 l 6.326,8.349 c 8.675,' +
          '11.444 24.209,12.532 36.784,5.586 11.46,-6.331 23.083,-9.758 34,' +
          '-9.758 18.107,0 28.294,7.919 28.294,20.75 v 0.375 c 0,16.224998 ' +
          '-15.469,39.410998 -59.231,43.180998 l -1.507,1.697 c -0.832,0.93' +
          '6 0.218,13.212 2.339,27.413 l 1.741,11.58 c 2.121,14.201 14.065,' +
          '25.71 26.668,25.71 12.603,0 23.839,-5.406 25.08,-12.069 1.256,-6' +
          '.668 2.268,-12.075 2.268,-12.075 41.869,-10.192 73.567992,-43.56' +
          '1 73.567992,-91.097998 z"'#10'     id="path2-8" /><path'#10'     style="' +
          'fill:none;stroke:#000000;stroke-width:15;stroke-miterlimit:4;str' +
          'oke-dasharray:none;stroke-opacity:1"'#10'     d="m -206.75447,219.12' +
          '261 c -14.359,0 -25.993,11.64 -25.993,25.999 v 12.14 c 0,14.359 ' +
          '11.64,25.999 25.993,25.999 h 22.322 c 14.359,0 25.999,-11.64 25.' +
          '999,-25.999 v -12.14 c 0,-14.359 -11.645,-25.999 -25.999,-25.999' +
          ' z"'#10'     id="path4-7" /></g>'#10'<g'#10'   id="g8">'#10'</g>'#10'<g'#10'   id="g10">' +
          #10'</g>'#10'<g'#10'   id="g12">'#10'</g>'#10'<g'#10'   id="g14">'#10'</g>'#10'<g'#10'   id="g16">'#10 +
          '</g>'#10'<g'#10'   id="g18">'#10'</g>'#10'<g'#10'   id="g20">'#10'</g>'#10'<g'#10'   id="g22">'#10'<' +
          '/g>'#10'<g'#10'   id="g24">'#10'</g>'#10'<g'#10'   id="g26">'#10'</g>'#10'<g'#10'   id="g28">'#10'</' +
          'g>'#10'<g'#10'   id="g30">'#10'</g>'#10'<g'#10'   id="g32">'#10'</g>'#10'<g'#10'   id="g34">'#10'</g' +
          '>'#10'<g'#10'   id="g36">'#10'</g>'#10'</svg>'#10
        Opacity = 1.000000000000000000
      end
      item
        MultiResBitmap = <
          item
          end>
        IconName = '01'
        SVGText = 
          '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'#10'<!-- Gene' +
          'rator: Adobe Illustrator 18.1.1, SVG Export Plug-In . SVG Versio' +
          'n: 6.00 Build 0)  -->'#10#10'<svg'#10'   version="1.1"'#10'   id="Capa_1"'#10'   x' +
          '="0px"'#10'   y="0px"'#10'   viewBox="0 0 281.232 281.232"'#10'   style="ena' +
          'ble-background:new 0 0 281.232 281.232;"'#10'   xml:space="preserve"' +
          #10'   sodipodi:docname="01.svg"'#10'   inkscape:version="1.1 (c68e22c3' +
          '87, 2021-05-23)"'#10'   xmlns:inkscape="http://www.inkscape.org/name' +
          'spaces/inkscape"'#10'   xmlns:sodipodi="http://sodipodi.sourceforge.' +
          'net/DTD/sodipodi-0.dtd"'#10'   xmlns="http://www.w3.org/2000/svg"'#10'  ' +
          ' xmlns:svg="http://www.w3.org/2000/svg"><defs'#10'   id="defs41">'#10#9#10 +
          #9#10'</defs><sodipodi:namedview'#10'   id="namedview39"'#10'   pagecolor="#' +
          'ffffff"'#10'   bordercolor="#666666"'#10'   borderopacity="1.0"'#10'   inksc' +
          'ape:pageshadow="2"'#10'   inkscape:pageopacity="0.0"'#10'   inkscape:pag' +
          'echeckerboard="0"'#10'   showgrid="false"'#10'   inkscape:zoom="1.479205' +
          '8"'#10'   inkscape:cx="98.701613"'#10'   inkscape:cy="187.60067"'#10'   inks' +
          'cape:window-width="1920"'#10'   inkscape:window-height="1017"'#10'   ink' +
          'scape:window-x="-8"'#10'   inkscape:window-y="32"'#10'   inkscape:window' +
          '-maximized="1"'#10'   inkscape:current-layer="Capa_1" />'#10'<g'#10'   id="g' +
          '1058"'#10'   inkscape:export-filename="D:\download\01.png"'#10'   inksca' +
          'pe:export-xdpi="90"'#10'   inkscape:export-ydpi="90"><path'#10'     styl' +
          'e="fill:#010002"'#10'     d="M 231.634,79.976 V 79.225 C 231.634,30.' +
          '181 192.772,0 137.32,0 105.333,0 79.905,9.018 59.536,22.98 47.69' +
          '5,31.095 46.629,48.886 55.304,60.335 l 6.326,8.349 c 8.675,11.44' +
          '4 24.209,12.532 36.784,5.586 11.46,-6.331 23.083,-9.758 34,-9.75' +
          '8 18.107,0 28.294,7.919 28.294,20.75 v 0.375 c 0,16.225 -15.469,' +
          '39.411 -59.231,43.181 l -1.507,1.697 c -0.832,0.936 0.218,13.212' +
          ' 2.339,27.413 l 1.741,11.58 c 2.121,14.201 14.065,25.71 26.668,2' +
          '5.71 12.603,0 23.839,-5.406 25.08,-12.069 1.256,-6.668 2.268,-12' +
          '.075 2.268,-12.075 41.869,-10.192 73.568,-43.561 73.568,-91.098 ' +
          'z"'#10'     id="path2" /><path'#10'     style="fill:#010002"'#10'     d="m 1' +
          '18.42,217.095 c -14.359,0 -25.993,11.64 -25.993,25.999 v 12.14 c' +
          ' 0,14.359 11.64,25.999 25.993,25.999 h 22.322 c 14.359,0 25.999,' +
          '-11.64 25.999,-25.999 v -12.14 c 0,-14.359 -11.645,-25.999 -25.9' +
          '99,-25.999 z"'#10'     id="path4" /></g>'#10'<g'#10'   id="g8">'#10'</g>'#10'<g'#10'   i' +
          'd="g10">'#10'</g>'#10'<g'#10'   id="g12">'#10'</g>'#10'<g'#10'   id="g14">'#10'</g>'#10'<g'#10'   id' +
          '="g16">'#10'</g>'#10'<g'#10'   id="g18">'#10'</g>'#10'<g'#10'   id="g20">'#10'</g>'#10'<g'#10'   id=' +
          '"g22">'#10'</g>'#10'<g'#10'   id="g24">'#10'</g>'#10'<g'#10'   id="g26">'#10'</g>'#10'<g'#10'   id="' +
          'g28">'#10'</g>'#10'<g'#10'   id="g30">'#10'</g>'#10'<g'#10'   id="g32">'#10'</g>'#10'<g'#10'   id="g' +
          '34">'#10'</g>'#10'<g'#10'   id="g36">'#10'</g>'#10'</svg>'#10
        Opacity = 1.000000000000000000
      end>
    Destination = <
      item
        Layers = <
          item
            Name = '02'
            SourceRect.Right = 120.000000000000000000
            SourceRect.Bottom = 120.000000000000000000
          end>
      end
      item
        Layers = <
          item
            Name = '01'
            SourceRect.Right = 30.000000000000000000
            SourceRect.Bottom = 30.000000000000000000
          end>
      end
      item
        Layers = <>
      end>
    Left = 96
    Top = 56
  end
end
