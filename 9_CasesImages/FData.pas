unit FData;

interface

uses
  System.SysUtils, System.Classes, System.ImageList, FMX.ImgList,
  FMX.SVGIconImageList;

type
  TMyDatas = class(TDataModule)
    SVGIconImageList1: TSVGIconImageList;
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  MyDatas: TMyDatas;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

end.
