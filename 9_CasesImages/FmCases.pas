unit FmCases;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, FmCase;

type
  TOnClickEvent = procedure(Sender: TObject; x, y: Integer) of object;

  TFrmCases = class(TFrame)
    recFond: TRectangle;
  private
    FLines: Integer;
    FColumns: Integer;
    FOnClick: TOnClickEvent;
    FCases: array of array of TFrmCase;
    function GetSizeImg: Integer;
    procedure OnClickCase(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Lines: Integer read FLines;
    property Columns: Integer read FColumns;
    property SizeImage: Integer read GetSizeImg;
  published
    property OnClick: TOnClickEvent read FOnClick write FOnClick;
  end;

implementation

{$R *.fmx}

uses
  FMX.Platform;

{ TFrmCases }

constructor TFrmCases.Create(AOwner: TComponent);
var
  lig, col: Integer;
  cas: TFrmCase;
begin
  inherited;

  FColumns := Trunc(Self.Size.Width) div Self.SizeImage;
  FLines := Trunc(Self.Size.Height) div Self.SizeImage;
  SetLength(FCases, FLines, FColumns);

  FOnClick := nil;

  Self.Size.Width := Columns * Self.SizeImage;
  Self.Size.Height := Lines * Self.SizeImage;

  for lig := 0 to Lines - 1 do
    for col := 0 to Columns - 1 do
    begin
      cas := TFrmCase.Create(Self);
      cas.Name := Format('case_%d_%d', [lig, col]);
      cas.Parent := Self;
      cas.Size.Height := Self.SizeImage;
      cas.Size.Width := Self.SizeImage;
      cas.Position.x := col * Self.SizeImage;
      cas.Position.y := lig * Self.SizeImage;

      cas.OnClickImage := OnClickCase;
      FCases[col, lig] := cas;
    end;
end;

destructor TFrmCases.Destroy;
begin
  //
  inherited;
end;

function TFrmCases.GetSizeImg: Integer;
const
  SIZE_IMG = 24;
begin
  Result := SIZE_IMG;
end;

procedure TFrmCases.OnClickCase(Sender: TObject);
var
  x, y: Integer;
begin
  if Assigned(FOnClick) then
  begin
    x := Trunc(TFrmCase(Sender).Position.x) div SizeImage;
    y := Trunc(TFrmCase(Sender).Position.y) div SizeImage;
    FOnClick(Self, x, y);
  end;
end;

end.
