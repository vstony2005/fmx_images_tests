unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FmCases,
  FMX.Layouts, FMX.Objects, FMX.Controls.Presentation, FMX.StdCtrls;

type
  TForm5 = class(TForm)
    ScrollBox1: TScrollBox;
    FrmCases1: TFrmCases;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure ShowClick(Sender: TObject; x, y: Integer);
  public
  end;

var
  Form5: TForm5;

implementation

{$R *.fmx}

procedure TForm5.FormCreate(Sender: TObject);
begin
  FrmCases1.OnClick := ShowClick;
  Label1.Text := '-';
end;

procedure TForm5.FormShow(Sender: TObject);
begin
  FrmCases1.Size.Height := FrmCases1.Lines * FrmCases1.SizeImage;
  FrmCases1.Size.Width := FrmCases1.Columns * FrmCases1.SizeImage;
end;

procedure TForm5.ShowClick(Sender: TObject; x, y: Integer);
begin
  Label1.Text := Format('%d-%d', [x, y]);
end;

end.
