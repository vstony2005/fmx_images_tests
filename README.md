# FMX Images tests

The purpose of this project is to test how images work in an FMX project.
I am currently using Delphi 10.4.2.

## Project

### ShowImage

Tests to display an image from the components and from the code.

### MoveImage

Tests to move and animate images in a window.

### MultiImage

Tests to display an image from several images in a TImageList.

### SplitChange

Tests to add images in a TImageList and display them.

### Spritesheet

Animation creation tests from spritesheet with [Retro Animated BOOM Explosion](https://cartoonsmart.com/retro-animated-boom-explosion-1/).

### Animation of a character

Display of the different animations of a character with [Lucifer 4 Direction Warrior - Pixel Art](https://foozlecc.itch.io/lucifer-4-direction-warrior-pixel-art-free).

### Move of a character

Animate the movement of a character with [Lucifer 4 Direction Warrior - Pixel Art](https://foozlecc.itch.io/lucifer-4-direction-warrior-pixel-art-free).

### IconList

Test use of the library: [IconFontsImageList-3.0.0-VCL-FMX-](https://github.com/EtheaDev/IconFontsImageList)

### Cases Images

Test use of the library: [SVGIconImageList-2.5.0-VCL-FMX-](https://github.com/EtheaDev/SVGIconImageList)

### Change Page

Test an application with several pages. Change of page with an animation.
2 versions of the main page.
The first one contains all the components used to display all the windows.
For the second version, the components are created in the code.

### Question Marsk

Use and simple customization of a TPath.

## Images Used

For these tests, I used [Kenney's animal images](https://kenney.nl/assets/animal-pack-redux);
